package id.anomstudio.belajarislam.ui.menu.beranda.edit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.anomstudio.belajarislam.R;
import id.anomstudio.belajarislam.model.ArtikelModel;
import id.anomstudio.belajarislam.ui.MainActivity;

public class EditActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.titleToolbar)
    TextView titleToolbar;
    @BindView(R.id.text_kategori)
    TextView text_kategori;
    @BindView(R.id.kategori)
    MaterialSpinner input_kategori;
    @BindView(R.id.judul)
    EditText input_judul;
    @BindView(R.id.deskripsi)
    EditText input_deskripsi;
    @BindView(R.id.add)
    ImageButton add;
    private DatabaseReference databaseReference;
    private String id,kategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);
        text_kategori.setVisibility(View.GONE);
        input_kategori.setVisibility(View.GONE);
        databaseReference = FirebaseDatabase.getInstance().getReference("artikels");
        input_kategori.setItems("Fikih Doa dan Dzikir", "Kumpulan Doa dan Dzikir", "Biografi Narasumber");
        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        kategori = bundle.getString("kategori");
        input_judul.setText(bundle.getString("judul"));
        input_deskripsi.setText(bundle.getString("deskripsi"));
    }

    @OnClick(R.id.back)
    public void back(){
        onBackPressed();
    }

    @OnClick(R.id.add)
    public void add(View view){
        String kategoris = kategori;
        String judul = input_judul.getText().toString();
        String deskripsi = input_deskripsi.getText().toString();

        if(!TextUtils.isEmpty(kategori) && !TextUtils.isEmpty(judul) && !TextUtils.isEmpty(deskripsi)){

            ArtikelModel artikelModel = new ArtikelModel(id,kategoris,judul,deskripsi);
            databaseReference.child(kategoris).child(id).setValue(artikelModel);
            hapusKetikaSama(kategoris);
//            if(kategoris.equals("Fikih Doa dan Dzikir")){
//                databaseReference.child(kategoris).child(id).setValue(artikelModel);
//                hapusKetikaSama(kategoris);
//            }else if(kategoris.equals("Kumpulan Doa dan Dzikir")){
//                databaseReference.child(kategoris).child(id).setValue(artikelModel);
//                hapusKetikaSama(kategoris);
//            }else if(kategoris.equals("Biografi Narasumber")){
//                databaseReference.child(kategoris).child(id).setValue(artikelModel);
//                hapusKetikaSama(kategoris);
//            }
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
            finish();

        }else{
            AlertDialog.Builder alertEdit = new AlertDialog.Builder(EditActivity.this, R.style.customAlertDialog);
            alertEdit.setMessage("Artikel belum terisi semua");
            alertEdit.setCancelable(false);
            alertEdit.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertEdit.show();
        }
    }

    private void hapusKetikaSama(String kategoris){
        if(!kategori.equals(kategoris)){
            databaseReference.child(kategori).child(id).removeValue();
        }
    }

}
