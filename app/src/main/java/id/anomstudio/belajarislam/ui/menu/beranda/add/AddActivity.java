package id.anomstudio.belajarislam.ui.menu.beranda.add;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.anomstudio.belajarislam.R;
import id.anomstudio.belajarislam.model.ArtikelModel;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.titleToolbar)
    TextView titleToolbar;
    @BindView(R.id.kategori)
    MaterialSpinner input_kategori;
    @BindView(R.id.judul)
    EditText input_judul;
    @BindView(R.id.deskripsi)
    EditText input_deskripsi;
    @BindView(R.id.add)
    ImageButton add;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);
        ButterKnife.bind(this);
        databaseReference = FirebaseDatabase.getInstance().getReference("artikels");
        input_kategori.setItems("Fikih Doa dan Dzikir", "Kumpulan Doa dan Dzikir", "Biografi Narasumber");
    }

    @OnClick(R.id.back)
    public void back(){
        onBackPressed();
    }

    @OnClick(R.id.add)
    public void add(View view){

        String id = databaseReference.push().getKey();
        String kategori = input_kategori.getText().toString();
        String judul = input_judul.getText().toString();
        String deskripsi = input_deskripsi.getText().toString();

        if(!TextUtils.isEmpty(kategori) && !TextUtils.isEmpty(judul) && !TextUtils.isEmpty(deskripsi)){

            ArtikelModel artikelModel = new ArtikelModel(id,kategori,judul,deskripsi);
            if(kategori.equals("Fikih Doa dan Dzikir")){
                databaseReference.child(kategori).child(id).setValue(artikelModel);
            }else if(kategori.equals("Kumpulan Doa dan Dzikir")){
                databaseReference.child(kategori).child(id).setValue(artikelModel);
            }else if(kategori.equals("Biografi Narasumber")){
                databaseReference.child(kategori).child(id).setValue(artikelModel);
            }
            onBackPressed();
            finish();

        }else{
            AlertDialog.Builder alertEdit = new AlertDialog.Builder(AddActivity.this, R.style.customAlertDialog);
            alertEdit.setMessage("Artikel belum terisi semua");
            alertEdit.setCancelable(false);
            alertEdit.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertEdit.show();
        }
    }


}
